﻿using System.Collections;
using System.Collections.Generic;
using Others;
using UnityEngine;

public class ShootableMonster : Monster
{
    [SerializeField] private float rate = 2F;
    private Bullet _bullet;
    [SerializeField]
    private Color bulletColor = Color.white;
    protected override void Awake()
    {
        _bullet = Resources.Load<Bullet>("Bullet");
    }

    protected override void Start()
    {
        InvokeRepeating("Shoot",rate,rate);
    }

    private void Shoot()
    {
        Vector3 position = transform.position;

        position.y += 0.5F;
        Bullet newBullet = Instantiate(_bullet, position, _bullet.transform.rotation);
        newBullet.Parent = gameObject;
        newBullet.Direction = -newBullet.transform.right;
        newBullet.Color = bulletColor;
        
    }

    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        Unit unit = collider.GetComponent<Unit>();
        
        if (unit && unit is Character)
        {
            if (Mathf.Abs(unit.transform.position.x - transform.position.x) < 0.68f)
            {
                ReceiveDamage();
            }
            else
            {
                unit.ReceiveDamage();
            }
        }
    }
}

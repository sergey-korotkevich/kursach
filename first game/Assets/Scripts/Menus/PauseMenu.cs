﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public string levelSelect;

    public string mainMenu;
   
    private bool isPaused;

    public GameObject pauseMenuCanvas;
    // Start is called before the first frame update
   

    // Update is called once per frame
   

    void Update()
    {
        if (isPaused)
        {
            pauseMenuCanvas.SetActive(true);
            
        }
        else
        {
            pauseMenuCanvas.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = true;
        }
        
    }

    public void Resume()
    {
        isPaused = false;
    }

    public void LevelSelect()
    {
        Application.LoadLevel(levelSelect);
    }

    public void Quit()
    {
        Application.LoadLevel(0);
    }
}

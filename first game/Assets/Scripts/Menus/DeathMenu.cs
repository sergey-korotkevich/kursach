﻿using System;
using System.Collections;
using System.Collections.Generic;
using Others;
using UnityEngine;

public class DeathMenu : MonoBehaviour
{
    public string levelSelect;
    private Character character;
    public string mainMenu;
   
   

    [SerializeField] private GameObject deathMenuCanvas;
    private LivesBar _livesBar;

    private void Awake()
    {
       
        deathMenuCanvas.SetActive(false);
    }

    void Update()
    {
        int check = 0;
        if (check == 0)
        {
            if (character == null)
            {
                character = FindObjectOfType<Character>();
                check = 1;
            }
        }

        if (character.Lives == 0f)
        {
            deathMenuCanvas.SetActive(true);
        }
       
        
    }

    public void Restart()
    {
        Application.LoadLevel(1);
    }

    public void LevelSelect()
    {
        Application.LoadLevel(levelSelect);
    }

    public void Quit()
    {
        Application.LoadLevel(0);
    }
}
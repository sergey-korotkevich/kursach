﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{

   [SerializeField] private GameObject platform;

   [SerializeField]   private float speed;

   [SerializeField] private Transform currentPoint;

   [SerializeField]   private Transform[] points;

   [SerializeField]  private int pointSelection;
    
    // Start is called before the first frame update
    void Start()
    {
        currentPoint = points[pointSelection];
    }

    // Update is called once per frame
    void Update()
    {
        platform.transform.position = Vector3.MoveTowards(platform.transform.position, currentPoint.position, Time.deltaTime * speed);
        if (platform.transform.position == currentPoint.position)
        {
            pointSelection++;
            if (pointSelection == points.LongLength)
            {
                pointSelection = 0;
                
            }

            currentPoint = points[pointSelection];
        }

       
    }
}

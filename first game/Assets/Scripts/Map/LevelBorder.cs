﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBorder : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Character character = FindObjectOfType<Character>();
        Unit unit = collider.GetComponent<Unit>();

        if (unit && unit is Character)
        {

            character.Lives = 0;
        }
    }
}

﻿using UnityEngine;

namespace Others
{
    public class Heart : MonoBehaviour
    {
    
        private void OnTriggerEnter2D(Collider2D collider)
        {
            Character character = collider.GetComponent<Character>();
            if (character)
            {
                character.Lives++;
                Destroy(gameObject);
            }
        }
    }
}

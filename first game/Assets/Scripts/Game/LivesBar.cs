﻿using System;
using UnityEngine;

namespace Others
{
    public class LivesBar : MonoBehaviour
    {
        private int check = 0;
        private Transform[] hearts = new Transform[5];
        private Character _character;
        private void Awake()
        {
            _character = FindObjectOfType<Character>();
       
            for (int i = 0; i < hearts.Length; i++)
            {
                hearts[i] = transform.GetChild(i);
            }
        }

        private void Update()
        {
            if (check == 0)
            {
                if (_character == null)
                {
                    _character = FindObjectOfType<Character>();

                    _character = FindObjectOfType<Character>();
                    check = 1;
                }
            }
        }

        public void Refresh()
        {
            for (int i = 0; i < hearts.Length; i++)
            {
                if (i < _character.Lives) hearts[i].gameObject.SetActive(true);
                else hearts[i].gameObject.SetActive(false);
            }
        }
    }
}

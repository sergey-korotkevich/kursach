﻿using UnityEngine;

namespace Others
{
   public class Bullet : MonoBehaviour
   {
  
      private Vector3 _direction;
      private GameObject parent;
      public Color Color
      {
         set { _sprite.color = value;
         }
      }
   
      public GameObject Parent
      {
         set { parent = value; }
      }
   
      public Vector3 Direction
      {
         set { _direction = value; }
      }
      public float speed = 10.0f;
      private SpriteRenderer _sprite;

      private void Awake()
      {
         _sprite = GetComponentInChildren<SpriteRenderer>();
      }


      private void Update()
      {
         transform.position = Vector3.MoveTowards(transform.position, transform.position + _direction, speed * Time.deltaTime);
      }

      private void Start()
      {
         Destroy(gameObject,1.5f);
      }
   
      private void OnTriggerEnter2D(Collider2D collider)
      {

         Unit unit = collider.GetComponent<Unit>();
         if (unit && unit.gameObject != parent)
         {
            if (unit is Character)
            {
               unit.ReceiveDamage();
            }

            Destroy(gameObject);
         }
      }
   }
}

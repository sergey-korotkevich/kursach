﻿using UnityEngine;

namespace Others
{
    public class CameraController : MonoBehaviour
    {
        private Character _character;
        [SerializeField]
        private float speed = 2f;

        [SerializeField] private Transform target;

        private void Awake()
        {
            if (!target) target = FindObjectOfType<Character>().transform;
        }

        private void Update()
        {
           
           
                Vector3 position = target.position;
                position.z = -10;
                transform.position = Vector3.Lerp(transform.position, position, speed * Time.deltaTime);
            
        
        }
    }
}

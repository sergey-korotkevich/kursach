﻿using UnityEngine;

namespace Others
{
    public class Finish : MonoBehaviour
    {
        protected virtual void OnTriggerEnter2D(Collider2D collider)
        {
            Unit unit = collider.GetComponent<Unit>();
        
            if (unit && unit is Character)
            {
                Application.LoadLevel(0);
            }

        
        }
        // Update is called once per frame
        void Update()
        {
        
        }
    }
}

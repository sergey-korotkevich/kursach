﻿using System;
using System.Collections;
using System.Collections.Generic;

using Others;

using UnityEngine;
using UnityEngine.Networking;

public class Character : Unit
{
    [SerializeField] private Vector3 direction;
    [SerializeField] private Vector2Int _direction;
    [SerializeField] private bool isGrounted;
    [SerializeField] private float speed = 3.0f;
    [SerializeField] private AudioSource jumpAudio;
    [SerializeField] private AudioSource receiveDamageAudio;
    [SerializeField] private AudioSource shootAudio;
    [SerializeField] private float lives = 5;
    [SerializeField] private bool check;
    [SerializeField] private float jumpForce = 15.0F;
    
    
    
   

   
    private void Start()
    {
        const byte code = 101;
       
        direction = new Vector3(transform.position.x,transform.position.y);
        
    }

    public float Lives
    {
        get
        {
            return lives;
        }
        set
        {
            if (value <= 5) lives = value;
            _livesBar.Refresh();
        }
    }

    private LivesBar _livesBar;
    
    private Bullet _bullet;
    private CharState State
    {
        get
        {
            return (CharState) _animator.GetInteger("State");
        }
        set
        {
            _animator.SetInteger("State",(int) value);
        }
    }
    
    private Rigidbody2D _rigidbody;
    private Animator _animator;
    private SpriteRenderer _sprite;

    private void Awake()
    {
        _livesBar = FindObjectOfType<LivesBar>();
        _bullet = Resources.Load<Bullet>("Bullet");
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _sprite = GetComponentInChildren<SpriteRenderer>();
    }

    private void FixedUpdate()
    {
       
    }


    private void Shoot()
    {
        shootAudio.Play();
        Vector3 position = transform.position;
        
        position.y += 0.8f;
        
        Bullet newBullet = Instantiate(_bullet, position, _bullet.transform.rotation);

        newBullet.Parent = gameObject;
        
        newBullet.Direction = newBullet.transform.right * (_sprite.flipX ? -1.0F : 1.0F);
    }
    
    private void Update()
    {
     
            CheckGround();
            
            if (lives == 0f)
            {
                Destroy(gameObject);

            }

           
            
            if (Input.GetButtonDown("Fire1"))
            {
                Shoot();
            }

            if (isGrounted)
            {
                State = CharState.Idle;
            }
            if (Input.GetButton("Horizontal"))
            {
                direction = transform.right * Input.GetAxisRaw("Horizontal");
        
                transform.position = Vector3.MoveTowards(transform.position, transform.position + direction,speed * Time.deltaTime );

                if (Input.GetKey(KeyCode.A))
                {
                    _direction = Vector2Int.right;
                } 
                if (Input.GetKey(KeyCode.D))
                {
                    _direction = Vector2Int.left;
                }
                

                
                if(isGrounted)State = CharState.Run;
                
            }
            if (_direction == Vector2Int.left)
            {
                _sprite.flipX = false;
            }
            if (_direction == Vector2Int.right)
            {
                _sprite.flipX = true;
            }

            if ( Input.GetButtonDown("Jump") && isGrounted)
            {
            
                Jump();
                check = false;
           
            
            
               
            
            }
            if (Input.GetButtonDown("Jump")  && !check && !isGrounted)
            {
                Jump();
                check = true;
            }
            
        
        
        

      
    }

    private void Die()
    {
        
    } 
    
    private void Run()
    {
       
    }

    public override void ReceiveDamage()
    {
        receiveDamageAudio.Play();
        Lives--;
        Debug.Log(lives);
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.AddForce(transform.up * 5 , ForceMode2D.Impulse);
    }

    private void Jump()
    {
        _rigidbody.velocity = new Vector2(0,jumpForce);
        jumpAudio.Play();
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {

        Unit unit = collider.gameObject.GetComponent<Unit>();
        /*if(unit) ReceiveDamage();*/

        
        

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.CompareTag("Moving Platform"))
        {
            transform.parent = other.transform;
        }
    } private void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform.CompareTag("Moving Platform"))
        {
            transform.parent = null;
        }
    }

    private void CheckGround()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.4F);

        isGrounted = colliders.Length > 1; //Необычный, но комплексный if
        
        if (!isGrounted) State = CharState.Jump;
    }
}

public enum CharState
{
    Idle,
    Run,
    Jump
}

